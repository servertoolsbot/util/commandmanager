package com.gitlab.servertoolsbot.util.commandmanager;

import java.lang.annotation.*;

@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Command {
    String name();
    String[] aliases() default "";
    String description() default "";
    String usage() default "";
    String assertPermission() default "";
    String noPermissionMethod() default "noPermission";
    String permissionCheckMethod() default "performPermissionCheck";
}
