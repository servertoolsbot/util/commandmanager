package com.gitlab.servertoolsbot.util.commandmanager;

import com.gitlab.servertoolsbot.util.messagemanager.MessageManager;
import com.gitlab.servertoolsbot.util.permissionmanager.PermissionManager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CommandManager {
    private String prefix = "";
    private MessageManager messageManagerInstance;
    private PermissionManager permissionManagerInstance;
    private List<Class<? extends BasicCommand>> registeredCommands = new ArrayList<>();

    public CommandManager() {

    }

    public void handleInput(String input) {
        registeredCommands.forEach(cmd -> {
            BasicCommand cmdClass = null;
            try {
                cmdClass = cmd.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
                return;
            }
            if (cmdClass == null) return;
            for (String commandName : cmdClass.getAllCommandNames()) if (input.startsWith(getPrefix()) && input.split(" ")[0].replace(getPrefix(), "").equalsIgnoreCase(commandName)) cmdClass.handleInput(input);
        });
    }

    public void registerMessages() {
        List<Method> methodsToCall = new ArrayList<>();
        this.getRegisteredCommands().forEach(clazz -> {
            List<Method> methodList = Arrays.stream(clazz.getMethods()).filter(method -> {
                if (method.getName().equalsIgnoreCase("addMessages")) {
                    return true;
                }
                return false;
            }).collect(Collectors.toList());
            if (methodList.size() > 0) {
                methodsToCall.addAll(methodList);
            }
        });
        methodsToCall.forEach(method -> {
            try {
                method.invoke(getClass().newInstance(), this.getMessageManagerInstance());
            } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
                e.printStackTrace();
            }
        });
    }

    public CommandManager registerCommand(BasicCommand aClass) {
        registeredCommands.add(aClass.getClass());
        return this;
    }
    public CommandManager registerCommands(BasicCommand... classes) {
        for (BasicCommand aClass : classes) registerCommand(aClass);
        return this;
    }

    public CommandManager registerCommand(Class<? extends BasicCommand> aClass) {
        registeredCommands.add(aClass);
        return this;
    }
    public CommandManager registerCommands(Class<? extends BasicCommand>... classes) {
        for (Class<? extends BasicCommand> aClass : classes) registerCommand(aClass);
        return this;
    }

    public List<Class<? extends BasicCommand>> getRegisteredCommands() {
        return this.registeredCommands;
    }

    public CommandManager setPrefix(String prefix) {
        this.prefix = prefix;
        return this;
    }
    public String getPrefix() {
        return this.prefix;
    }
    public CommandManager setMessageManagerInstance(MessageManager messageManagerInstance) {
        this.messageManagerInstance = messageManagerInstance;
        return this;
    }
    public MessageManager getMessageManagerInstance() {
        return this.messageManagerInstance;
    }
    public CommandManager setPermissionManagerInstance(PermissionManager permissionManagerInstance) {
        this.permissionManagerInstance = permissionManagerInstance;
        return this;
    }
    public PermissionManager getPermissionManagerInstance() {
        return this.permissionManagerInstance;
    }
}
