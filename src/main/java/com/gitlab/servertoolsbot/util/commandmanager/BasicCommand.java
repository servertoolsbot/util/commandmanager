package com.gitlab.servertoolsbot.util.commandmanager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class BasicCommand {
    protected List<String> param = new ArrayList<>();

    protected BasicCommand() {
        this.param = getParamWithoutFlags(this.param);
    }

    public void handle() {
    }

    public void handleInput(String input) {
        if (!getCommandAssertPermission().equals("")) {
            try {
                Method performCheck = getClass().getMethod(getCommandPermissionCheckMethod(), String.class);
                Boolean result = (Boolean)performCheck.invoke(getClass().newInstance(), getCommandAssertPermission());
                if (!result) {
                    getClass().getMethod(getCommandNoPermissionMethod()).invoke(getClass().newInstance());
                    return;
                }
            } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
                e.printStackTrace();
                return;
            }
        }

        param = Arrays.asList(input.split(" "));
        handle();

        HashMap<String, Method> commandFlags = getCommandFlags();
        for (int i = 0; i < param.size(); i++) {
            if (param.get(i).startsWith("--")) {
                String flag = param.get(i).replace("--", "");
                if (!flag.isEmpty()) {
                    if (commandFlags.containsKey(flag)) {
                        if ((i + 1) < param.size()) {
                            String value = param.get(i + 1);

                            this.param = getParamWithoutFlags(this.param);

                            try {
                                commandFlags.get(flag).invoke(getClass().newInstance(), value);
                            } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
                                e.printStackTrace();
                            }
                            commandFlags.remove(flag);
                        }
                    }
                }
            }
        }



        if (!commandFlags.isEmpty()) {
            HashMap<String, Method> commandFlagsMissing = getCommandFlagsMissing();
            commandFlags.forEach((flag, method) -> commandFlagsMissing.forEach((missingFlag, missingMethod) -> {
                if (flag.equals(missingFlag)) {
                    try {
                        commandFlagsMissing.get(flag).invoke(getClass().newInstance());
                    } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
                        e.printStackTrace();
                    }
                }
            }));
        }

        try {
            HashMap<Integer, Method> commandParameters = getCommandParameters();
            Method toCall = commandParameters.get(0);
            if (!((param.size() - 1) < 0 || (param.size() - 1) > commandParameters.keySet().size())) {
                int biggestAcceptedInt = param.size() - 1;
                if (commandParameters.keySet().size() > 1) biggestAcceptedInt = new ArrayList<>(commandParameters.keySet()).get(commandParameters.keySet().size() - 1);
                if (!((param.size() - 1) > biggestAcceptedInt)) toCall = getCommandParameters().get(param.size() - 1);
                else toCall = getCommandParameters().get(biggestAcceptedInt);
            }
            toCall.invoke(getClass().newInstance());
        } catch (IllegalAccessException | InvocationTargetException | InstantiationException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    public String getCommandName() {
        return getClass().getAnnotation(Command.class).name();
    }
    public List<String> getCommandAliases() {
        return Arrays.asList(getClass().getAnnotation(Command.class).aliases());
    }
    public List<String> getAllCommandNames() {
        List<String> allNames = new ArrayList<>();
        allNames.add(getCommandName());
        allNames.addAll(getCommandAliases());
        return allNames;
    }
    public String getCommandDescription() {
        return getClass().getAnnotation(Command.class).description();
    }
    public String getCommandUsage() {
        return getClass().getAnnotation(Command.class).usage();
    }
    public String getCommandAssertPermission() {
        return getClass().getAnnotation(Command.class).assertPermission();
    }
    public String getCommandNoPermissionMethod() {
        return getClass().getAnnotation(Command.class).noPermissionMethod();
    }
    public String getCommandPermissionCheckMethod() {
        return getClass().getAnnotation(Command.class).permissionCheckMethod();
    }

    public HashMap<Integer, Method> getCommandParameters() {
        HashMap<Integer, Method> list = new HashMap<>();
        for (Method method : getClass().getMethods()) {
            if (method.isAnnotationPresent(CommandParam.class)) list.put(method.getAnnotation(CommandParam.class).value(), method);
        }
        HashMap<Integer, Method> toReturn = new HashMap<>();
        list.entrySet().stream().sorted(Map.Entry.comparingByKey(Comparator.reverseOrder())).forEachOrdered(m -> toReturn.put(m.getKey(), m.getValue()));
        return toReturn;
    }
    public HashMap<String, Method> getCommandFlags() {
        HashMap<String, Method> list = new HashMap<>();
        for (Method method : getClass().getMethods()) if (method.isAnnotationPresent(CommandFlag.class)) list.put(method.getAnnotation(CommandFlag.class).value(), method);
        return list;
    }
    public HashMap<String, Method> getCommandFlagsMissing() {
        HashMap<String, Method> list = new HashMap<>();
        for (Method method : getClass().getMethods()) if (method.isAnnotationPresent(CommandFlagMissing.class)) list.put(method.getAnnotation(CommandFlagMissing.class).value(), method);
        return list;
    }

    public List<String> getParamWithoutFlags(List<String> param) {
        String total = "";
        for (int it = 0; it < param.size(); it++) {
            if (!(it > 0 && (param.get(it).startsWith("--") || param.get(it - 1).startsWith("--")))) {
                if (it == 0) total = total + param.get(it);
                else total = total + " " + param.get(it);
            }
        }
        return Arrays.asList(total.split(" "));
    }


    @CommandParam(0)
    public void noArgumentsProvided() {

    }

    public Boolean performPermissionCheck(String permission) {
        return true;
    }

    public void noPermission() {

    }
}
