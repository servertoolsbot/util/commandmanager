package com.gitlab.servertoolsbot.util.commandmanager;

import java.lang.annotation.*;

@Inherited
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CommandFlagMissing {
    String value();
}
