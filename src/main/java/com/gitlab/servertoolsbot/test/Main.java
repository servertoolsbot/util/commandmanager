package com.gitlab.servertoolsbot.test;

import com.gitlab.servertoolsbot.util.commandmanager.CommandManager;
import com.gitlab.servertoolsbot.util.messagemanager.Message;
import com.gitlab.servertoolsbot.util.messagemanager.MessageManager;

class Main {
    public static void main(String[] args) {
        System.out.println("Ahhh!!1");
        CommandManager manager = new CommandManager();
        MessageManager msgmanager = new MessageManager();
        msgmanager.addMessage(new Message("command.help.usage", "This command can do much things! I guess"));
        msgmanager.load();
        manager.setMessageManagerInstance(msgmanager);

        manager.registerCommand(HelpCommand.class);
        manager.registerCommand(TestCommand.class);

        manager.getRegisteredCommands().forEach(clazz -> {
            try {
                System.out.println("Command usage of" + clazz.newInstance().getCommandName() + ":");
                System.out.println(clazz.newInstance().getCommandUsage());
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        });
    }
}
