package com.gitlab.servertoolsbot.test;

import com.gitlab.servertoolsbot.util.commandmanager.BasicCommand;
import com.gitlab.servertoolsbot.util.commandmanager.Command;
import com.gitlab.servertoolsbot.util.commandmanager.CommandParam;

@Command(name = "test", usage = "test")
class TestCommand extends BasicCommand {
    @CommandParam(2)
    public void didThing() {
        System.out.println("Hey!!1");
    }
}
