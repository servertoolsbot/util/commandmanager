package com.gitlab.servertoolsbot.test;

import com.gitlab.servertoolsbot.util.commandmanager.*;
import com.gitlab.servertoolsbot.util.messagemanager.MessageManager;

@Command(name = "help", description = "Provides help", aliases = {"some","thing"})
class HelpCommand extends BasicCommand {


    @Override
    @CommandParam(0)
    public void noArgumentsProvided() {
        param.forEach(par -> System.out.println("param: "+par));
        System.out.println("par.size(): "+param.size());
        System.out.println("Why? I mean we have arguments!");
    }

    @CommandParam(1)
    public void smth() {
        System.out.println("Argument one IS set");
    }

    @CommandParam(2)
    public void test() {
        System.out.println("Argument two IS set");
    }

    @CommandParam(3)
    public void thing() {
        System.out.println("argument three IS set");
    }

    @CommandFlag("test")
    public void flagOneHandling(String value) {
        System.out.println("From flag: " + value);
    }

    @CommandFlagMissing("test")
    public void something() {
        System.out.println("Missing flag: test");
    }

    public String getUsage(MessageManager messageManager) {
        return messageManager.getMessage("command."+getCommandName() + ".usage");
    }
}
